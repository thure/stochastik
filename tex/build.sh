#!/bin/bash

# TODO: trap on exit, ok like this?
#       proper handling of temp file, using mktemp or similar

errorhandler() {

    rm -f --interactive='never' X 2>/dev/null
    exit 99 # do not remove this line.
    # cf. http://www.davidpashley.com/articles/
    # writing-robust-shell-scripts.html
    # 2013-07-02
}

trap errorhandler INT TERM # wenn das skript abschmiert oder
# abgebrochen wird,
# funktion errorhandler aufrufen

viewer=evince
errors='Error|error|Fatal|Undefined|undefined|may have changed|Extra alignment tab|Extra \}, or forgotten \$|Missing \$ inserted|Missing \} inserted|extra \}|superscript|subscript'
warnings='Undefined|undefined|may have changed|Reference'

usage() {
    echo
    echo "Usage: $0"
    echo '    '"to build stochastik.pdf and delete intermediate files"
    echo
    echo "Usage: $0 clean"
    echo '    '"to clean all generated files"\
         "but not the PDF"
    echo
    echo "Usage: $0 cleanfull"'   or    '"$0 cf"
    echo '    '"to clean all generated files beginning with 'stochastik'"
    echo
    echo "Usage: $0 open"
    echo '    '"to open the PDF using $viewer"
}

list() {
    ls -laFh
}

clean() {
    local basename=${1%.tex}
    rm -r --interactive='never'\
       "${basename}.log" *.{aux,out,toc} X\
       2>/dev/null
}

cleanfull() {
    clean "$1"
    local basename=${1%.tex}
    rm -r --interactive='never' "${basename}.pdf" 2>/dev/null
}

checkfile () {
    echo

    if [ -d "$1" ]; then
        echo 'Whuuuut!? You gave me an entire directory?!'; exit 1
    fi
    
    if [ ! -f "$1" ]; then
        echo 'No such file!';
        echo
        echo "Say"'    '"$0 --help"'    for details.'
        exit 1
    fi

    if [[ ! "$1" == *.tex ]]; then
        # http://stackoverflow.com/a/407334
        echo 'File name does not end in .tex!'; exit 2
    fi

    if ! grep -q LaTeX <(file -L "$1"); then
        echo 'This is probably not a LaTeX file!'; exit 3
    fi

    if ! grep -Fq '\end{document}' "$1"; then
        echo 'Missing "\end{document}".'\
             'Won'"'"'t run pdflatex.'; exit 4
    fi

    if tail -n1 "$1" | grep "%"; then
	echo 'The last line shouldn'"'"'t be commented!'; exit 5
    fi     
}

firstrun() {
    pdflatex -draftmode -interaction=nonstopmode\
             -file-line-error "$1" > /dev/null
    local result="$?"
    echo "$result" > X
}

otherruns() {
    pdflatex -interaction=nonstopmode "$1" > /dev/null
}

grepwarn() {
    true # todo, um doppelten code in der fkt. work im fall
    # if [ "$(cat X)" == 0 ]
    # zu vermeiden
}

work() {
    echo; echo "$1 given as input file"; sleep 0.2
    checkfile "$1"
    basename=${1%.tex}
    ok=false
    log="${basename}.log"
    echo -n "First run "
    spinner firstrun "$1"
    if [ "$(cat X)" == 0 ]; then
        ok=true
        for i in 2 3 4; do
            [ "$i" -eq 2 ] && echo
            echo -ne '\n      '"Run #$i"' '
            spinner otherruns "$1"
        done
        echo; echo
	clean
        if grep -E 'tex:' "$log" | grep -E "$warnings"; then
            while read line; do
                yellow '      '"$line"
            done < <(grep -E 'tex:' "$log" | grep -E "$warnings" | head -n 5)
            echo -e "\n\nSee the log file $(yellow $log) for details."
            echo
        fi        
        open "${basename}.pdf" &
        exit 0
    else
        echo -ne '\b\b\b\b\b\b\b\b\b' # 'completed' überschreiben
        echo "$(yellow $(bold FAILED)) --"\
             "the first problems are summarised below."; echo
        while read line; do
            yellow '      '"$line"
        done < <(grep -E 'tex:' "$log" | grep -E "$errors" | head -n 5)
        echo -e "\n\nSee the log file $(yellow $log) for details."
        echo
    fi
    rm -f --interactive='never' X 2>/dev/null
    exit 1
}

spinner() {
    # http://unix.stackexchange.com/a/276558
    # http://stackoverflow.com/a/12498305
    ("$@") &
    pid="$!" # Process ID of the previous running command
    
    spin[0]="-"; spin[1]="\\"; spin[2]="|"; spin[3]="/"
    echo -n "${spin[0]}"
    while kill -0 "$pid" 2>/dev/null
    do
        for i in "${spin[@]}"
        do
            echo -ne "\b$i"
            sleep 0.1
        done
    done
    echo -ne "\bcompleted"
}

open() {

    if ! type "$viewer" > /dev/null; then
        echo "$viewer not found."
        exit 2
    fi
    echo Using "$viewer" to open "$1"
    "$viewer" "$1" &> /dev/null
    # sleep 1
    # wmctrl -i -a "$WINDOWID"
    # http://stackoverflow.com/a/24327736
    # wie kriegt die shell wieder den Fokus?
}

yellow() {
    local yellow='\e[0;93m'
    local normal='\e[0m'
    echo -ne "${yellow}"    # echo -n: Kein Zeilenumbruch am Ende
    echo -nE "$@"           # echo -e: Escape-Sequenzen interpretieren
    echo -e "${normal}"     # echo -e: Escape-Sequenzen nicht interpretieren
}

# tput zu benutzen wäre nicht schlecht. Damit kriegt man aber nur ein
# blasses Gelb und kein Kanariengelb hin.
# Außerdem werden Zeilenumbrüche verschluckt, vermutlich wegen des
#    echo -nE "$@"

bold() {
    local bold=$(tput bold)
    local normal=$(tput sgr0) 
    echo -ne "${bold}"        # auf Fettschrift umschalten
    echo -nE "$@"             # Text ausgeben
    echo -e "${normal}"       # auf normale Schrift umschalten
}

# http://stackoverflow.com/questions/5947742/
# how-to-change-the-output-color-of-echo-in-linux
# 2017-01-07

cd main || { echo "cd failed" ; exit 2 ; }

case "$#" in

    0)
        if [ -f stochastik.tex ]
        then
            work stochastik.tex
        else
            echo 'Please give me a *.tex file to work with!';
	    read -p 'You can do so now.:   ' path;
            work "$path"
        fi
        ;;

    1)
        case "$1" in
            '--help')
                usage
                exit 0
                ;;
            
            clean)
                [ -f 'clean.tex' ] && echo 'warning: clean.tex exists'
                clean stochastik.tex
                exit 0
                ;;

            cleanfull|cf)
                [ -f 'clean.tex' ] && echo 'warning: clean.tex exists'
                cleanfull stochastik.tex
                exit 0
                ;;

            *)
                work "$1"
                ;;
        esac
        ;;

    *)
        usage
        ;;
esac

trap - INT TERM EXIT # traps wieder loeschen
