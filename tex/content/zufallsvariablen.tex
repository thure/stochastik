\section{Zufallsvariablen}

\begin{wrapfigure}{r}{0.5\textwidth}
% \begin{wrapfigure}[10]{r}[0pt]{0.5\textwidth}
  \begin{center}
    \vspace*{-14mm} % ugly hack
    \begin{tikzpicture}[scale=3]

\newcounter{a}
\newcounter{b}
\foreach \p/\t in {10/3, 10/2, 10/4, 10/3, 10/4, 
                   10/3, 10/2, 10/4, 10/1, 10/4}
  {
    \setcounter{a}{\value{b}}
    \addtocounter{b}{\p}
    \slice{\thea/100*360}
          {\theb/100*360}
          % {\t}{\p\%}
          {\t}{}
  }
\end{tikzpicture}

% http://www.texample.net/tikz/examples/pie-chart/
% 2017-02-01

  \end{center}
  \caption{Glücksrad (man denke sich den Pfeil obendrüber in der
    Mitte)}
  % zu großer vspace unter wrapfigure -.-
\end{wrapfigure}

% https://www.sharelatex.com/learn/Wrapping_text_around_figures
% 2017-02-01

Drehen wir das abgebildete Glücksrad, so erwarten wir, dass in etwa
$10\%$ der Fälle die~1, in etwa $20\%$ der Fälle die~2, in etwa $30\%$
der Fälle die~3, in etwa $40\%$ der Fälle die~4 angezeigt
wird.\footnote{Dass nicht jeder Spieler das Rad gleich stark antreibt,
wollen wir hier vernachlässigen.}
Wir können dies aufgrund der symmetrischen Bauweise, der gleichen
Größe der Felder etc.\ annehmen. Da es sich aber um ein Glücksrad
handelt, wollen wir den Ausgängen, also den Elementen der Menge
${\Omega}=\set{1,\,2,\,3,\,4}$, wie folgt Auszahlungsbeträge zuordnen:

  \vspace{4mm}
    \begin{tabular}{cc}
      \toprule
      Angezeigte Zahl & Auszahlungsbetrag \\
      \midrule
      1               & \EUR{12,00} \\
      2               & \EUR{1,50}  \\
      3               & \EUR{1,00}  \\
      4               & \EUR{0,50}  \\
      \bottomrule
    \end{tabular}
  \vspace{4mm}

Außerdem verlangen wir von jedem Spieler einen Einsatz von~\EUR{2}.

Wir erkennen, dass ein Spieler danach trachten wird, die~1 zu
erdrehen, weil der ausgezahlte Betrag dann hoch ist; andererseits
ist die~1 nur in jedem zehnten Spiel zu erwarten.

Da die Wahrscheinlichkeit, die~1 zu erdrehen, $10\%$, also
$\frac{1}{10}$, beträgt, liegt es nahe, den Auszahlungsbetrag mit
$\frac{1}{10}$ zu multiplizieren, denn wenn wir einfachheitshalber
annehmen, dass beim Drehen einer anderen Zahl als~1 nichts ausgezahlt
wird, so haben wir eine \emph{mittlere} Auszahlung von \EUR{1.20}. In
unserem Beispiel erhalten wir so eine erwartete Auszahlung von
\[
   \text{\EUR{12}}\cdot \frac{1}{10} \ +\
   \text{\EUR{1.50}}\cdot\frac{1}{5} \ +\ 
     \text{\EUR{1}}\cdot\frac{3}{10} \ +\ 
   \text{\EUR{0.50}}\cdot \frac{2}{5}\  =\ \text{\EUR{2}}\quad,
\]

und die hebt sich mit dem Einsatz gerade auf. Im \enquote{langjährigen
Mittel} ist das Spiel also fair, und als Betreiber verdienen wir
nichts daran.

Wir haben hier eine Abbildung $\Omega\to\bbr$ angegeben, wenn wir, und
das wird sich auch im Folgenden als nützlich erweisen, die Einheiten
ignorieren.\footnote{Wir hätten in der Tabelle problemlos
  \enquote{Auszahlungsbetrag in Euro} als Überschrift der zweiten
  Spalte schreiben können, und hätten dann tatsächlich eine Abbildung
  $\Omega\to\bbr$ erhalten.}

Wir nennen eine solche Abbildung eine \textbf{diskrete reelle
  Zufallsvariable}, im Folgenden auch kurz
Zufallsvariable. \enquote{Diskret} bedeutet, dass die Menge~$\Omega$
abzählbar ist, und \enquote{reell} bedeutet, dass die Abbildung~$X$
die reellen Zahlen oder eine Teilmenge davon als Zielbereich hat. Wir
können also auch ${\Omega=\setwhere{\omega_i}{i\in I}}$ schreiben,
wobei $I$ eine abzählbare Indexmenge ist. Wir wollen außerdem
annehmen, dass ${X(\omega_i)=x_i}$ für alle $i\in I$ gilt, dass
also die Liste ${(x_1,\,x_2,\ldots,\,x_n)}$ angibt, auf welchen
Wert die einzelnen Ergebnisse des Zufallsexperiments jeweils
abgebildet werden. Dies können wir auch durch
${X(\Omega)=\setwhere{x_i}{i\in I}}$ ausdrücken.

Ist $X$ eine Zufallsvariable, so können wir ihren
\textbf{Erwartungswert}, das obige Beispiel verallgemeinernd, als
\begin{equation}
\label{deferwartungswert}
  E(X) = \Sum_{i\in I}x_i\cdot P(X=x_i)
\end{equation}

definieren. Die Schreibweise \enquote{$X=x_i$} ist dabei so zu
verstehen, dass wir alle Ergebnisse betrachten, denen unter der
Abbildung~$X$ der Wert $x_i$ zugeordnet wird. 

Eine alternative, eingängigere Schreibweise ist
\begin{equation}
\label{deferwartungswertzwei}
  E(X) = \Sum_{\omega\in \Omega}\omega\cdot P(X=\omega)\quad,
\end{equation}

die allerdings nur zulässig ist, falls $\Omega$ eine Teilmenge der reellen
Zahlen ist. Häufig codiert man jedoch Ausgänge eines
Zufallsexperiments durch natürliche Zahlen, so dass diese
Voraussetzung erfüllt ist.

Da die Summe im Falle von unendlichem $I$ nicht zu existieren
braucht, muss auch $E(X)$ nicht zwingend existieren. Wir wollen aber
im Folgenden davon ausgehen, dass die Summe in~(\ref{deferwartungswert})
existiert.

TODO: Varianz!

Werfen wir beispielsweise einen Würfel zweimal, so können wir die
beiden Würfe anhand ihrer Reihenfolge unterscheiden. Es wird also
${\Omega=[6]\times [6]}$ sinnvoll sein. Wir dürfen außerdem
${P(\set{(i,\,j)})=\frac{1}{36}}$ für alle $i,\,j\in [6]$ annehmen, denn
alle~36 Ausgänge sind gleichwahrscheinlich. Interessieren wir uns nun
für die Augensumme, so betrachten wir die Zufallsvariable
${X\colon \Omega\to\set{2,\ldots,12}}$.

Nachdem es nur eine Möglichkeit gibt, die Augensumme~2 zu
realisieren~--~nämlich mittels des Ereignisses ${\set{(1,\,1)}}$~--,
es jedoch bereits sechs Ergebnisse für ${X=7}$ gibt~--~nämlich
${(1,\,6)}$, ${(2,\,5)}$, ${(3,\,4)}$, ${(4,\,3)}$, ${(5,\,2)}$,
${(6,\,1)}$~--, wird die Zufallsvariable $X$ nicht mehr gleichverteilt
sein. Wir können dennoch die Werte ${P(X=2)=\frac{1}{36}}$ und
${P(X=7)=\frac{6}{36}=\frac{1}{6}}$ angeben.\footnote{Hier wird
  strenggenommen das Funktionssymbol $P$ in zwei Bedeutungen
  verwendet, einmal zur Angabe der Wahrscheinlichkeit der Ereignisse,
  ein anderes Mal zur Angabe der Wahrscheinlichkeiten, dass eine
  Zufallsvariable einen bestimmten Wert annimmt. Man kann diesem
  Dilemma entkommen, wenn man sich die Sprechweise \enquote{${X=2}$
    tritt mit Wahrscheinlichkeit~$\frac{1}{36}$ ein} oder \enquote{$X$
    nimmt den Wert~$2$ mit Wahrscheinlickeit~$\frac{1}{36}$ an}
  angewöhnt, also~\enquote{${P(\ldots)=\ldots}$} als \enquote{Die
    Wahrscheinlichkeit für~$\ldots$ ist~$\ldots$} liest. Alternativ
  betrachtet man die Funktion ${P\circ X^{-1}}$, um die Verteilung einer
  Zufallsvariablen anzugeben.}

In derselben Weise (durch Auszählen der Möglichkeiten) gelangen wir
zur in Abbildung~\ref{fig:verteilungzweiwuerfel} angegebenen
Wahrscheinlichkeitsverteilung. Abbildung~\ref{fig:histzweiwuerfel}
stellt diese Verteilung in Form eines \textbf{Histogramms} dar.
Dazu TODO: schreiben, wie man ein Histogramm zeichnet.

\begin{figure}[ht]
    \centering
    \begin{floatrow}
      \ffigbox[\FBwidth]{\caption{Verteilung}\label{fig:verteilungzweiwuerfel}}{%
        {
\begin{small}
\renewcommand{\arraystretch}{0.8}

    \begin{tabular}{cc}
      \toprule
      Augen- & Wahrschein- \\ summe & lichkeit \\ % ugly hack
      \midrule
      2               & $1/36$ \\
      3               & $2/36$ \\
      4               & $3/36$ \\
      5               & $4/36$ \\
      6               & $5/36$ \\
      7               & $6/36$ \\
      8               & $5/36$ \\
      9               & $4/36$ \\
      10              & $3/36$ \\
      11              & $2/36$ \\
      12              & $1/36$ \\
      \bottomrule
    \end{tabular}
\end{small}
}
      }
      \ffigbox[\FBwidth]{\caption{Histogramm}\label{fig:histzweiwuerfel}}{%

        %%%%%%%%%%%%%%%%% Histogramm
% \vspace*{10mm} % ugly hack, does not work
\pgfplotsset{
  compat=newest,
  xlabel near ticks,
  ylabel near ticks
}
  \begin{tikzpicture}[font=\small]
    \begin{axis}[
      ybar,
      width=10.3cm,
      height=6cm,
      bar width=13pt,
      xlabel={Augensumme beim zweimaligen Würfeln},
      ylabel={Wahrscheinlichkeit},
      ymin=0,
      ytick=data,
      xtick=data,
      yticklabels={0,$\frac{1}{36}$,
                     $\frac{2}{36}$,
                     $\frac{3}{36}$,
                     $\frac{4}{36}$,
                     $\frac{5}{36}$,
                     $\frac{6}{36}$},
      axis x line=bottom,
      axis y line=left,
      %enlarge x limits=1.0,
      xticklabel style={anchor=base,yshift=-\baselineskip},
      nodes near coords={$\pgfmathprintnumber
                          [fixed,precision=3]
                          \pgfplotspointmeta$}
    ]
      \addplot[fill=white] coordinates {
        %(0,0)
        (1,0)
        (2,1/36)
        (3,2/36)
        (4,3/36)
        (5,4/36)
        (6,5/36)
        (7,6/36)
        (8,5/36)
        (9,4/36)
        (10,3/36)
        (11,2/36)
        (12,1/36)
        (13,0)
      };
    \end{axis}
  \end{tikzpicture}

% nach http://tex.stackexchange.com/a/123867
% sowie http://tex.stackexchange.com/a/19591
% sowie http://tex.stackexchange.com/a/130645
% sowie http://tex.stackexchange.com/a/37595
% 2017-02-02

      }
    \end{floatrow}
  \end{figure}

\subsection{Binomialverteilung}

Sei $X\colon\Omega\to\bbr$ eine Zufallsvariable. Wenn $X$ nur die
Werte 0 und~1 annimmt und es
$p\in\intervalcc{0}{1}$ derart gibt, dass $X$ den Wert 1 mit
Wahrscheinlichkeit $p$ annimmt und den Wert $0$ mit Wahrscheinlichkeit
$1-p$, so heißt $X$ \textbf{Bernoulli-verteilt zum Parameter $p$}.

Das Standardbeispiel ist der Wurf einer Münze (die nicht fair sein
muss, denn $p$ kann durchaus von~$\frac{1}{2}$ verschieden sein).

Werfen wir dieselbe Münze mehrfach und zählen, wie oft Kopf oben
liegt, so codieren wir zunächst die Ausgänge des Experiments, indem
wir \enquote{0} für Zahl und \enquote{1} für Kopf schreiben. Wenn wir
$n$~Mal werfen, so kann die Zufallsvariable, die die Anzahl der
Ausgänge \enquote{Kopf} zählt, Werte aus der Menge
$[n]_0$ annehmen. Es mögen nun genau ${k\in [n]_0}$ Einsen
auftreten. Es ist dann unerheblich, in welcher Reihenfolge dies
geschieht; es kommt nur auf die Anzahl der Einsen an. 

Wenn wir die $n$ Münzwürfe als unterscheidbar ansehen (was zum
Beispiel dadurch geschehen kann, dass wir ihre \emph{Reihenfolge}
beachten), so haben wir es also mit einer $0$-$1$-Folge der Länge~$n$
zu tun, die wir in der Form
\[
   (x_1,\,x_2,\ldots,x_n)\qquad\qquad\text{mit
     $x_i\in\set{0,\,1}$ für alle $i\in[n]_0$}
\]
schreiben können. Wenn es genau $k$ Einsen gibt, so gibt es genau
${n-k}$ Nullen, und die Wahrscheinlichkeit für das Auftreten einer
solchen Folge ist nach der ersten Pfadregel (vgl.\
S.~\pageref{erstepfadregel}) gleich $p^k \cdot (1-p)^{n-k}$. Wenn wir
diese Wahrscheinlichkeit gemäß der zweiten Pfadregel mit der Anzahl
der möglichen Pfade multiplizieren, die genau~$k$ Einsen enthalten, so
haben wir die Gesamtwahrscheinlichkeit für das Auftreten von genau~$k$
Einsen ermittelt.~-- Wie viele Pfade der Länge~$n$ mit genau~$k$
Einsen gibt es? Wenn eine $0$-$1$-Folge der
Länge~$n=((n-k)+k)$ vorliegt, so gibt es $((n-k)+k)!$ verschiedene
Anordnungen der Ziffern; jedoch sind \emph{de facto} davon $k!$
ununterscheidbar, weil nur die Einsen permutiert werden, und für jede
solche Permutation gibt es $(n-k)!$ Permutationen, in denen nur die
Nullen permutiert werden. Damit gibt es
\[
   \dbinom{n}{k} := \Frac{n!}{k!\cdot(n-k)!}
\]
unterscheidbare $0$-$1$-Folgen der
Länge~$n$.\footnote{\url{https://de.wikipedia.org/w/index.php?title=Binomialkoeffizient&oldid=161305584\#Der_Binomialkoeffizient_in_der_Kombinatorik}}
% Diese URL enthält ein Doppelkreuz, das mittels eines Backslashes
% maskiert werden muss, damit TeX es nicht als Parameter einer
% parametrisierten Kontrollsequenz auffasst. Merkwürdigerweise muss
% das Kaufmanns-Und in der URL nicht maskiert werden...
% Es ist außerdem zu beachten, dass URLs sehr zerbrechlich sind,
% d.h. sie dürfen in der Eingabedatei nicht mit Zeilenumbrüchen auf
% mehrere Zeilen aufgeteilt werden -- selbst dann nicht, wenn diese
% Zeilenumbrüche durch Backslashes maskiert sind.

Wir bezeichnen $\tbinom{n}{k}$ als \textbf{Binomialkoeffizienten}.
Der Vollständigkeit halber vereinbaren wir noch
\[
   \dbinom{n}{0} =1\ ,\qquad \dbinom{n}{k}=0\quad\text{für alle
     $k,\,n\in\bbnn$ mit $n>k$}.
\]

Eine der wichtigsten Beziehungen, die der Binomialkoeffizient erfüllt,
ist der \textbf{binomische Lehrsatz}: Für alle $x,\,y\in\bbr$ und für
alle $n\in\bbnn$ gilt
\[
    (x+y)^n = \Sum_{k=0}^n\dbinom{n}{k}\cdot x^{k}\cdot y^{n-k}\quad,
\]
wovon man sich für jedes feste~$n\in\bbnn$ durch Ausmultiplizieren und
Zusammenfassen überzeugen kann; für allgemeines~$n$ bietet sich
vollständige Induktion an.

Insgesamt erhalten wir daher für jede zum
Parameter~$p\in\intervalcc{0}{1}$ binomialverteilte Zufallsvariable
und für alle $k\in[n]_0$:
\[
   P(X=k) = \dbinom{n}{k}\cdot p^k \cdot (1-p)^{n-k}\quad.
\]

Für den Erwartungswert erhalten wir $E(X)=n\cdot p$, wie aus der
Rechnung\footnote{\url{https://de.wikipedia.org/w/index.php?title=Binomialverteilung&oldid=161285187\#Erwartungswert}}
\begin{small}
%\[
\begin{align*}
   E(X) &= \Sum_{k\in [n]_0}k\cdot P(X=k)
            & \text{}\\
%
%
        &= \Sum_{k=0}^{n}k\cdot \dbinom{n}{k}
                         \cdot p^k \cdot (1-p)^{n-k}
            & \text{}\\
%
%
        &= \Sum_{k=0}^{n}k\cdot\Frac{n!}{k!\cdot(n-k)!}
                         \cdot p^k \cdot (1-p)^{n-k}
            & \text{Def. $\dbinom{n}{k}$}\\
%
%
        &= n\cdot\Sum_{k=0}^{n}k\cdot\Frac{(n-1)!}{k!\cdot(n-k)!}
                                \cdot p^k \cdot (1-p)^{n-k}
            & \text{Distributivität für $n$}\\
%
%
        &= n\cdot p\cdot\Sum_{k=0}^{n}k\cdot\Frac{(n-1)!}{k!\cdot(n-k)!}
                                \cdot p^{k-1} \cdot (1-p)^{n-k}
            & \text{Distributivität für $p$}\\
%
%
        &= n\cdot p\cdot\Sum_{k=1}^{n}\Frac{(n-1)!}{(k-1)!\cdot(n-k)!}
                                \cdot p^{k-1} \cdot (1-p)^{n-k}
            & \text{$0!=1$, $\Frac{k!}{k}=(k-1)!$}\\
%
%
        &= n\cdot p\cdot\Sum_{k=1}^{n}\Frac{(n-1)!}{(k-1)!\cdot(n-k)!}
                                \cdot p^{k-1} \cdot (1-p)^{n-1-(k-1)}
            & \text{$n-k=n-1-(k-1)$}\\
%
%
        &= n\cdot p\cdot\Sum_{k=1}^{n}\dbinom{n-1}{k-1}
                                \cdot p^{k-1} \cdot (1-p)^{n-1-(k-1)}
            & \text{Def.\ $\dbinom{n-1}{k-1}$}\\
%
%
        &= n\cdot p\cdot\Sum_{l=0}^{n-1}\dbinom{n-1}{l}
                                \cdot p^{l} \cdot (1-p)^{n-1-l}
            & \text{wenn $l:=k-1$ gesetzt wird}\\
%
%
        &= n\cdot p\cdot\Sum_{l=0}^{m}\dbinom{m}{l}
                                \cdot p^{l} \cdot (1-p)^{m-l}
            & \text{wenn $m:=n-1$ gesetzt wird}\\
%
%
        &= n\cdot p\cdot(p+(1-p))^m                                
            & \text{Binomischer Lehrsatz}\\
%
%
        &= n\cdot p\cdot1^m
            & \text{$p+(1-p)=1$}\\
%
%
        &= n\cdot p
\end{align*}
\end{small}
folgt. % Die Varianz ist 
\subsection{Normalverteilung}
 Hier kommt noch was!
