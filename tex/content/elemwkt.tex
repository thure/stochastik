\section{Elementare Wahrscheinlichkeitsrechnung}

\subsection{Grundbegriffe}

Zum Verständnis statistischer Methoden ist Vertrautheit mit den
Methoden der Wahrscheinlichkeitsrechnung hilfreich.

Der Aussage \enquote{Die Wahrscheinlichkeit, dass beim einmaligen Werfen
eines Würfels die 4 oben liegt, beträgt $\frac{1}{6}$} wird
sicherlich jeder zustimmen, weil es nur sechs mögliche Ergebnisse
gibt, die alle gleichwahrscheinlich sind.

Wir haben es in diesem einfachsten Fall mit dem \textbf{klassischen
  Wahrscheinlichkeitsbegriff} zu tun: Ein
\textbf{Laplace"=Experiment}~--~worunter wir einen Vorgang verstehen,
der, wenigstens im Prinzip, beliebig oft unter den gleichen
Bedingungen wiederholt werden kann und dessen Ergebnis nicht mit
Sicherheit vorherzusagen ist~--~hat nur endlich viele mögliche
\textbf{Ausgänge}, von denen keiner, etwa aus physikalischen Gründen,
gegenüber den anderen bevorzugt auftritt.

Kann man die Gleichverteilung nicht begründet annehmen, so spricht man
(auch allgemein) von einem \textbf{Zufallsexperiment}.

Jede \textbf{Realisierung} des Experiments hat genau einen Ausgang zum
Ergebnis. Um Wahrscheinlichkeiten bestimmen zu können, fassen wir die
möglichen Ausgänge des Experiments zur \textbf{Ergebnismenge}
zusammen, die traditionell mit dem griechischen Buchstaben~$\Omega$
bezeichnet wird. Im Beispiel des Würfels ist also
${\Omega=\set{1,\,2,\,3,\,4,\,5,\,6}}$. Man möchte nun nicht nur die
Wahrscheinlichkeit des Auftretens einzelner Augenzahlen betrachten,
sondern auch kompliziertere Sachverhalte untersuchen, etwa
\enquote{die geworfene Augenzahl ist größer als 2}. Dazu fasst man die
interessanten Ergebnisse, hier also die Ergebnisse 3, 4, 5, 6, zur
\textbf{Ereignismenge} $A=\set{3,\,4,\,5,\,6}$ zusammen, die eine
Teilmenge von $\Omega$ ist. Um die Wahrscheinlichkeit dafür zu
bestimmen, dass \textbf{das Ereignis $A$ eingetreten ist}, hat man nur
die Mächtigkeit von~$A$ durch die Mächtigkeit von~$\Omega$ zu
dividieren:
\label{vvv}
\[
P(A)=\Frac{\abs A}{\abs \Omega} = \Frac{4}{6} = \Frac{2}{3}\quad.
\]

Interpretieren können wir dies folgendermaßen: Wenn man einen Würfel
nur häufig genug wirft, so wird er in etwa $\frac{2}{3}$ der Fälle die
3, 4, 5, oder 6 zeigen.\footnote{Dass die \textbf{relative Häufigkeit}
    des Eintretens eines Ergebnisses aus der Menge $A$ gegen
    $\frac{2}{3}$ konvergiert, ist damit \emph{nicht} gesagt! Im
    Unterschied zu Grenzwerten können bei langen Versuchsreihen immer
    wieder einmal größere Abweichungen auftreten, denn, um es plakativ
  zu formulieren, \emph{ein Würfel hat kein Gedächtnis}~--~er kann
  nicht feststellen, dass in den letzten 300 Würfen die~5
  unterrepräsentiert war und \enquote{aufholen} muss.}
\begin{figure}[t]
  \centering
  \includegraphics[
% scale=5,
width=\textwidth,
]{../content/images/dilbert02.jpg}
\caption{Guckst du hier}
\label{yay}
\end{figure}

Die Schreibweise \enquote{$P(A)$} erinnert nicht zufällig an das
bekannte \enquote{$f(x)$}: wir können Wahrscheinlichkeiten auf"|fassen
als eine \textbf{Funktion}, die jedem möglichen \emph{Ereignis} eines
Zufallsexperiments eine Zahl zuordnet.  Wenn wir uns die Sprechweise
\enquote{In der \emph{Hälfte} aller Fälle ist die geworfene Augenzahl
  gerade} vor Augen halten~--~also
\enquote{${P(\set{2,\,4,\,6})=\frac{1}{2}}$} schreiben können
möchten~--, ist auch klar, wie wir vorgehen können: Wir nehmen
$\pot(\Omega)$ als Definitionsbereich\footnote{Tatsächlich können wir
  sogar etwas allgemeiner vorgehen, wenn wir den Begriff der
  \emph{$\sigma$"~Algebra} bemühen~--~im Fall überabzählbarer
  Grundmengen~$\Omega$ muss nicht mehr \emph{jede} Teilmenge
  von~$\Omega$ ein Ereignis bilden. Die Details überlassen wir den
  Hauptfach"=Mathematikern.} und das Intervall $\intervalcc{0}{1}$ als
Zielbereich: dem \textbf{unmöglichen Ereignis}~$\emptyset$ ordnen wir
die Wahrscheinlichkeit~0 zu, $P(\emptyset) = 0$, dem \textbf{sicheren
  Ereignis}~$\Omega$ erteilen wir die Wahrscheinlichkeit~1,
$P(\Omega) = 1$, und die Wahrscheinlichkeit eines beliebigen anderen
Ereignisses liegt dazwischen.

Etwas Vorsicht müssen wir aber doch walten lassen: Wenn wir einen
Würfel werfen, so wird es niemals passieren, dass die 2 und die 3
gleichzeitig oben liegen, die Ereignisse sind
\textbf{unvereinbar}. Andererseits erwarten wir, dass das Ereignis
$\set{2,\,3}$ doppelt so wahrscheinlich ist wie das Ereignis
$\set{2}$, weil es nun doppelt so viele \textbf{günstige Fälle}
gibt. Kurz, wir fordern, dass für alle Ereignisse
$A,\,B\subseteq \Omega$ gilt:
${A\intersection B=\emptyset\implies P(A\union B)=P(A)+P(B)}$, \ie
dass für alle Ereignisse $A,\,B\subseteq \Omega$ gilt: Sind $A$
und~$B$ unvereinbar, so ist die Wahrscheinlichkeit für das Auftreten
mindestens eines der Ereignisse $A$ und~$B$ gleich der Summe der
Einzelwahrscheinlichkeiten des Auftretens von $A$ bzw.\ $B$.

Die drei Forderungen
\begin{enumerate}
\item $0\leqs P(A)\leqs 1$ für alle $A\subseteq\Omega$,
\item $P(\Omega)=1$,
\item \label{kolgomorowdrei} $A\intersection B=\emptyset\implies
  P(A\union B)=P(A)+P(B)$ für alle $A,\,B\subseteq\Omega$
\end{enumerate}

sind als \textbf{Wahrscheinlichkeitsaxiome von
  Kolmogorow\footnote{Andrei Nikolajewitsch Kolmogorow, 1903--1987}}
bekannt. Eine Abbildung, die diese Forderungen erfüllt, bezeichnen wir
als \textbf{Wahrscheinlichkeitsmaß}\footnote{Ebenfalls verbreitet ist
  der Begriff \textbf{Wahrscheinlichkeitsverteilung}, der aber zu
  Verwechslungen mit der später definierten
  \textbf{Verteilungsfunktion} Anlass gibt.}.

Weitere Eigenschaften von Wahrscheinlichkeiten:\footnote{Teschl,
  Gerald; Teschl, Susanne: Mathematik für Informatiker, Band 2,
  Analysis und Statistik. 2. Auf"|l., Springer-Verlag
  Berlin Heidelberg 2007, Satz~26.11}
\begin{enumerate}
\item Es gilt $P(\overline A)=1-P(A)$ für alle $A\subseteq \Omega$.
\item Für
  alle $A,\,B\subseteq\Omega$ gilt:\quad $P(A\union B)=P(A)+P(B) -
  P(A\intersection B)$.
\item Sind die Ereignisse $A_1,\,\ldots,\,A_n$ \textbf{paarweise
    unvereinbar}, \ie gilt $A_j\intersection A_k=\emptyset$ für alle
  $j,\,k\in [n]$ mit $j\neq k$, so folgt
  \[
     P(A_1\union A_2\union \ldots \union A_n) = P(A_1) + P(A_2) +
     \ldots + P(A_n)\quad.
  \]
\end{enumerate}

\subsection{Bedingte Wahrscheinlichkeit}

Wenn wir einen Würfel werfen, so ist die Wahrscheinlichkeit, dass die
4 oben liegt, gleich~$\frac{1}{6}$. Was ändert sich, wenn wir bereits
wissen, dass die geworfene Augenzahl gerade ist?

Es ändert sich nicht der Würfel, es ändert sich unsere
Betrachtungsweise.  Wir haben es nun nicht mehr mit der Ergebnismenge
$\Omega=\set{1,\,2,\,3,\,4,\,5,\,6}$ zu tun, sondern beschränken uns
auf die kleinere Menge $B:=\set{2,\,4,\,6}$. Da jedes Ergebnis in
$\Omega$ gleichwahrscheinlich war, ist auch jedes Ergebnis in $B$
gleichwahrscheinlich. Wir können also wie zuvor mit der Formel der
klassischen Wahrscheinlichkeit \enquote{Anzahl der günstigen Ergebnisse
durch Anzahl der möglichen Ergebnisse} arbeiten und erhalten hier
eine Wahrscheinlichkeit von~$\frac{1}{3}$.  Dies legt Schreibweisen
wie $P_B(A)$ nahe, wenn wir $P_B$ als ein Wahrscheinlichkeitsmaß
${P_B\colon B\to\intervalcc{0}{1}}$ auffassen.\footnote{Dass diese
  Interpretation mit der weiter unten gebenenen Definition von
  $P(A\mid B)$ tatsächlich zulässig ist, ist eine einfache
  Übungsaufgabe.} Eine andere Möglichkeit ist die folgende: Wenn wir
wissen, \emph{dass} das Ereignis $B$ eingetreten ist und uns fragen,
wie wahrscheinlich dann das Eintreten von $A$ ist, so fragen wir nach
der Wahrscheinlichkeit dafür, dass $A$ und $B$ gemeinsam eintreten,
also für das Eintreten des Ereignisses $A\intersection B$, jedoch
nicht bezogen auf die Grundmenge $\Omega$, sondern bezogen auf die
Grundmenge $B$. Hierfür schreiben wir
\begin{equation}
  \label{eq:bedwkt}
  P_B(A) := P(A\mid B) := \Frac{P(A\intersection B)}{P(B)}
\end{equation}
und beachten, dass der Bruch nur definiert ist, falls $P(B) \neq 0$
ist, was aber unproblemtisch ist, da wir ja davon ausgehen, dass $B$
eingetreten ist, also eine von Null verschiedene Wahrscheinlichkeit
haben muss. Wir lesen \enquote{$P(A\mid B)$} als \enquote{$P$ von $A$,
gegeben $B$}.

Es bleibt zu klären, warum man ausgerechnet dividieren muss. 
Dazu betrachten wir den Spezialfall eines Laplace"=Experiments: Das
Ereignis $A$ hat $\abs A$ günstige gleichwahrscheinliche Ergebnisse,
das Ereignis $B$ hat $\abs B$ günstige gleichwahrscheinliche
Ergebnisse, das Ereignis $A\intersection B$ hat $\abs {A \intersection
  B}$ günstige gleichwahrscheinliche Ergebnisse. Tritt nun $B$ ein, so
fragen wir nach der Wahrscheinlichkeit, dass \emph{auch} $A$ eintritt;
wir haben also $\abs B$ \emph{mögliche} Ergebnisse, von denen
$\abs{A\intersection B}$ \emph{günstig} sind; es folgt
\[
   P(A\mid B) = \Frac{\abs{A\intersection B}}{\abs B} =
   \Frac{P(A\intersection B)}{P(B)}\quad.
\]

Formen wir die obige Definitionsgleichung um, so erhalten wir
\begin{equation}
  \label{eq:prodbedwkt}
  P(A\intersection B) = P(A\mid B)\cdot P(B)\quad,
\end{equation}
also: Die Wahrscheinlichkeit, dass $A$ und $B$ gemeinsam eintreten,
ist das Produkt aus der Wahrscheinlichkeit, dass $A$ eintritt, sofern
$B$ eintritt, und der Wahrscheinlichkeit, dass $B$ überhaupt
eintritt. % Einen Anhaltspunkt für die Richtigkeit der
% Formel~\eqref{eq:bedwkt} erhalten wir durch das folgende Beispiel: Ist
% $\Omega=\set{1,\,2,\,3,\,4,\,5,\,6}$, $A$ das Ereignis \enquote{Die
% geworfene Zahl ist 4} und $B$ das Ereignis \enquote{Die geworfene
% Zahl ist gerade}, so erhalten wir
% \[
% P(A\mid B) = \Frac{P(A\intersection B)}{P(B)} =
% \Frac{P(\set{4})}{P(\set{2,\,4,\,6})} = \Frac{1}{3}\quad,
% \]
% also denselben Wert, den wir bereits weiter oben ermittelt hatten, und
% der Bruch links davon, $\frac{P(\set{4})}{P(\set{2,\,4,\,6})}$, hat
% die bekannte Interpretation \enquote{Anzahl der günstigen Ergebnisse
% durch Anzahl der möglichen Ergebnisse}.

Um uns die Produktformel~\eqref{eq:prodbedwkt} zu veranschaulichen,
nehmen wir \textbf{Wahrscheinlichkeitsbäume} zu Hilfe.\footnote{Ein
  \emph{Baum} ist, wie durch die graphische Darstellung nahegelegt,
  ein zusammenhängender, kreisfreier Graph, \ie eine Ansammlung von
  (durch Punkte oder in unserem Fall Mengen) markierten \emph{Knoten},
  die durch \emph{Kanten} verbunden sind. Die Anzahl der an einem
  Knoten anliegenden Kanten ist der \emph{Grad} des Knotens. Die
  Knoten mit Grad~$1$ heißen \emph{Blätter}, alle anderen Knoten
  heißen \emph{innere Knoten}.}

\begin{figure}[h]
  \centering
% Set the overall layout of the tree
\tikzstyle{level 1}=[level distance=3.5cm, sibling distance=3.8cm]
\tikzstyle{level 2}=[level distance=3.5cm, sibling distance=2.5cm]
% Define styles for bags and leafs (sic!)
\tikzstyle{root} = [text width=0.8em, text centered]
\tikzstyle{bag} = [text width=0.8em, text centered]
\tikzstyle{end} = [minimum width=0pt, inner sep=0pt]

% The sloped option gives rotated edge labels. Personally
% I find sloped labels a bit difficult to read. Remove the sloped options
% to get horizontal labels.
\begin{tikzpicture}[grow=right, sloped]
\node[root] {$\Omega$} % root
    child {
        node[bag] {$\overline A$}        
            child {
                node[end, label=right:
                    {$\overline A\intersection \overline B$}] {}
                edge from parent
                node[above] {\brlab{$P(\overline B\mid\overline A)$}}
            }
            child {
                node[end, label=right:
                    {$\overline A\intersection B$}] {}
                edge from parent
                node[above] {\brlab{$P(B\mid\overline A)$}}
            }
            edge from parent 
            node[above] {\brlab{$P(\overline A)$}}
    }
    child {
        node[bag] {$A$}        
        child {
                node[end, label=right:
                    {$A\intersection\overline B$}] {}
                edge from parent
                node[above] {\brlab{$P(\overline B \mid A)$}}
            }
            child {
                node[end, label=right:
                    {$A\intersection B$}] {}
                edge from parent
                node[above] {\brlab{$P(B\mid A)$}}
            }
        edge from parent         
            node[above] {\brlab{$P(A)$}}
    };
\end{tikzpicture}
% http://www.texample.net/tikz/examples/probability-tree/
% 2017-01-30

  \caption{Einfacher Wahrscheinlichkeitsbaum}
  \label{fig:wktbaum}
\end{figure}

Darunter wollen wir Bäume verstehen, deren Knoten mit Ereignissen,
also Teilmengen von~$\Omega$, und deren Kanten mit den zugehörigen
Wahrscheinlichkeiten beschriftet sind. Den grundlegenden
Wahrscheinlichkeitsbaum für ein zweistufiges Zufallsexperiment zeigt
Abb.~\ref{fig:wktbaum}. Wir erkennen an diesem Baum:
\begin{enumerate}
\item Die Summe der Wahrscheinlichkeiten, die an den näher an den
  Blättern liegenden Kanten jedes inneren Knotens stehen,
  ist~$1$. (Das ist klar, denn wenn z.B.\ das Ereignis~$A$ eingetreten
  ist, muss, wenn wir das zweistufige Experiment vollständig
  durchführen wollen, eines der Ereignisse $B$,~$\overline B$
  eintreten, also das Ereignis~$B\union\overline B=\Omega$, und
  $P(\Omega)=1$.)
\item % Nachdem wir das Experiment vollständig durchgeführt haben, ist
  % genau eines der Ereignisse $A\intersection B$,
  % $A\intersection\overline B$, $\overline A\intersection B$,
  % $\overline A\intersection \overline B$ eingetreten.
  % Ist nun das Ereignis $A\intersection B$ eingetreten, so muss dazu
  % jedenfalls $A$ eingetreten sein, was mit der Wahrscheinlichkeit
  % $P(A)$ geschieht.
  Die Wahrscheinlichkeit eines Ereignisses ist gleich der Summe der
  Wahrscheinlichkeiten der für dieses Ereignis günstigen Pfade, wir
  verteilen die Gesamtwahrscheinlichkeit auf die einzelnen
  Pfade.\footnote{Diese wunderbar prägnante Formulierung stammt von
    Elke Warmuth,
    \url{http://didaktik.math.hu-berlin.de/files/pfadregeln_2010_h.pdf},
    abgerufen am 30.01.2017.} (Das können wir aus dem dritten
  Wahrscheinlichkeitsaxiom (vgl.\ S.~\pageref{kolgomorowdrei})
  folgern.) Dieses Ergebnis ist als \textbf{Satz von der totalen
    Wahrscheinlichkeit} oder als \textbf{zweite
    Pfadregel}\label{totalewkt} bekannt:\footnote{nach
    Barbara Langfeld, Mathematik C für Informatikstudierende,
    06.02.2014, Satz~6.2.5}
  \begin{quote} 
    Bilden die nichtleeren Ereignisse $A_1,\ldots,A_n$ eine Zerlegung
    von~$\Omega$, \ie sind sie paarweise disjunkt und gilt
    $\Union\set{A_1,\ldots,A_n}=\Omega$, so gilt für jedes
    Ereignis $B$:
    \[
       P(B) = \Sum_{i=1}^n P(B\mid A_i)\cdot P(A_i)\quad.
    \]
  \end{quote}
\item \label{erstepfadregel}\textbf{Erste Pfadregel.} Die
  Wahrscheinlichkeit eines Ereignisses ist gleich dem Produkt
  der Wahrscheinlichkeiten entlang der Kanten, die zu diesem Ereignis
  führen. Hier sehen wir beispielsweise
  ${P(A\intersection B)=P(A)\cdot P(B\mid A)}$. Wegen der
  Kommutativität von~$\cdot$ und von~$\intersection$ ist dies gleich
  $P(A\mid B)\cdot P(B)$ wie in~\eqref{eq:prodbedwkt}.
\end{enumerate}

TODO:
Wir haben uns die Produktformel
  veranschaulicht. Kann man sie besser begründen? Norbert Henze
  schreibt dazu:\footnote{Henze, Norbert: Stochastik für Einsteiger,
    Eine Einführung in die faszinierende Welt des Zufalls. 9. Auf"|l.,
    Vieweg+Teubner Verlag, 2012, S.~98}
\begin{quote}
  Sie sollten erkennen, dass die erste Pfadregel kein mathematischer
  Satz, sondern eine aus dem empirischen Gesetz über die
  Stabilisierung relativer Häufigkeiten entstandene Definition für die
  Wahrscheinlichkeit eines Ergebnisses (Pfades) im mehrstufigen
  Experiment ist.
\end{quote}


\subsection{Satz von Bayes}

Man lasse sich die folgende Schlagzeile auf der Zunge
zergehen:\footnote{Auch dieses Beispiel habe ich bei Elke Warmuth
  a.a.O.\ gefunden.}
\begin{quote}
\textsl
{
  Der Tod fährt mit! Vier von zehn tödlich verunglückten Autofahrern\\
  trugen keinen Sicherheitsgurt!
}
\end{quote}

Hier wird $P(\overline{\text{Gurt}}\mid\text{Tod})$ angegeben, also
die Wahrscheinlichkeit, dass man keinen Gurt getragen hatte, wenn
bekannt ist, dass man zu Tode kam~--~dabei wäre
$P(\text{Tod}\mid\overline{\text{Gurt}})$ viel aufschlussreicher: Wie
wahrscheinlich ist es, dass man zu Tode kommt, wenn bekannt ist, dass
man den Gurt nicht anlegt? Zunächst stehen diese beiden
Wahrscheinlichkeiten aber in keinem Zusammenhang.\footnote{In der Tat,
  selbst wenn \emph{jeder} tödlich verunglückte Autofahrer zum
  Zeitpunkt des letzten Unfalls angegurtet war, so ist nichts über all
  jene Fahrer ausgesagt, die an keinem Unfall beteiligt sind.}

Um diesen Zusammenhang herstellen zu können, müsste man wissen,
welcher Anteil der Autofahrer routinemäßig keinen Gurt trägt; diese Zahl
dürfte aber nur schwer zu bestimmen sein.

Nehmen wir ein einfacheres Beispiel her:\footnote{Barbara Langfeld,
  Mathematik C für Informatikstudierende, Klausur (Version A,
  1.~Prüfungszeitraum), 15.02.2013}
\begin{quote}
\textsl{
  Der Spamfilter einer großen Firma erkennt Spam-E-Mails in~$99\%$ der
  Fälle als solche. Andererseits gibt es $2\%$ vernünftige E-Mails,
  die irrtümlich als Spam markiert werden. Derzeit sind $10\%$ aller
  E-Mails, die bei der Firma eingehen, Spam.\\
  Berechnen Sie die Wahrscheinlichkeit dafür, dass eine als Spam
  markierte E-Mail tatsächlich Spam ist.
}
\end{quote}

Die Grundmenge $\Omega$ ist die Menge aller E-Mails, die die Firma
erreichen. Bezeichne $S$ das Ereignis, dass eine E-Mail Spam ist, und
$F$ das Ereignis, dass sie im Filter hängen bleibt. Dann ist
$P(S \mid F)$ gesucht. Insbesondere ist $P(F\mid S)=\frac{99}{100}$
gegeben. Außerdem wissen wir $P(F\mid \overline S)=\frac{1}{50}$,
$P(S)=\frac{1}{10}$, $P(\overline S)=1-P(S)=\frac{9}{10}$.  Damit
folgt
\begin{small}
\[
\renewcommand{\arraystretch}{2}
\begin{array}{rcllll}
  P(S\mid F) &=& \Frac{P(S\intersection F)}{P(F)}
                  &&& \text{wegen \eqref{eq:bedwkt}}\\
%
%
             &=& \Frac{P(F\intersection S)}{P(F)}
                  &&& \text{$\intersection$ kommutativ}\\
%
%
             &=& \Frac{P(F\mid S)\cdot P(S)}{P(F)}
                  &&& \text{wegen \eqref{eq:prodbedwkt}}\\
%
%
             &=& \Frac{P(F\mid S)\cdot P(S)}{P(F\mid S)\cdot P(S) +
                 P(F\mid\overline S)\cdot P(\overline S)}
                  &&& \text{Satz von der totalen Wkt}\\
%
%
             &=& \Frac{\frac{99}{100} \cdot
                 \frac{1}{10}}{\frac{99}{100} \cdot \frac{1}{10} +
                 \frac{1}{50}\cdot \frac{9}{10}}
                  &&& \text{}\\
%
%
             &=& \Frac{11}{13}\quad.
\end{array}
\]
\end{small}

Betrachten wir dieses Beispiel genauer, so haben wir im Wesentlichen
den Nenner $P(F)$ mithilfe des Satzes von der totalen
Wahrscheinlichkeit ausgedrückt. Das geht natürlich auch mit einer
feineren Zerlegung als $\set{S,\,\overline S}$ \footnote{nach Barbara
  Langfeld, Mathematik C für Informatikstudierende, 06.02.2014, Satz
  6.2.7}:
\begin{quote}
  \textbf{Satz von Bayes.} Bilden die nichtleeren Ereignisse
  $A_1,\ldots,A_n$ eine Zerlegung von~$\Omega$, so gilt für jedes
  $i\in[n]$ und jedes Ereignis $B$ mit $P(B)\neq 0$:
    \[
       P(A_i\mid B) = \Frac{P(A_i)\cdot P(B\mid A_i)}
                           {\sum_{i=1}^n P(A_i)\cdot P(B\mid A_i)}\quad.
    \]
\end{quote}

Zur Verdeutlichung geben wir beide Wahrscheinlichkeitsbäume an. Dabei
können wir den linken Baum, der zuerst nach Spam und Nichtspam
unterscheidet, komplett beschriften, indem wir ausnutzen, dass die
Summe der Wahrscheinlichkeiten von Ereignis und Gegenereignis
gleich~$1$ ist. In dem rechten Baum hingegen ist zunächst nichts
bekannt. Der Satz von Bayes liefert uns jedoch $P(S\given
F)=\frac{11}{13}$, wobei wir festhalten, dass sich dieser Wert
keineswegs offensichtlich aus den in der Aufgabenstellung gegebenen
Zahlen errechnen lässt. Wir können nun $P(\overline S\given
F)=\frac{2}{13}$ folgern; für die Ermittlung der weiteren
Pfadwahrscheinlichkeiten müssen wir den Satz von Bayes erneut
heranziehen.

In Abbildung~\ref{yay} auf Seite~\pageref{yay} finden wir....
In Formel~\ref{vvv} auf Seite~\pageref{vvv} finden wir....

\vspace*{3mm}

\begin{minipage}[l]{0.48\linewidth}
% Set the overall layout of the tree
\tikzstyle{level 1}=[level distance=3cm, sibling distance=3.5cm]
\tikzstyle{level 2}=[level distance=3cm, sibling distance=2cm]
% Define styles for bags and leafs (sic!)
\tikzstyle{root} = [text width=0.8em, text centered]
\tikzstyle{bag} = [text width=0.8em, text centered]
\tikzstyle{end} = [minimum width=0pt, inner sep=0pt]
\begin{tikzpicture}[grow=down,auto]

\node[root] {$\Omega$} % root
child {
        node[bag] {$S$}                
            child {
                node[end, label=below:
                    {$S\intersection F$}] {}
                edge from parent
                node[above] {\brlab{$\frac{99}{100}$}}
            }
            child {
                node[end, label=below:
                    {$S\intersection\overline F$}] {}
                edge from parent
                node[above] {\brlab{$\frac{1}{100}$}}
            }
        edge from parent         
            node[above] {\brlab{$\frac{1}{10}$}}
    }
child {
        node[bag] {$\overline S$}                    
            child {
                node[end, label=below:
                    {$\overline S\intersection F$}] {}
                edge from parent
                node[above] {\brlab{$\frac{1}{50}$}}
            }
            child {
                node[end, label=below:
                    {$\overline S\intersection \overline F$}] {}
                edge from parent
                node[above] {\brlab{$\frac{49}{50}$}}
            }
            edge from parent 
            node[above] {\brlab{$\frac{9}{10}$}}
    };

\end{tikzpicture}
\vspace*{3mm}
\captionof{figure}{Wahrscheinlichkeitsbaum aus der Aufgabenstellung}
\end{minipage}
\hspace{3mm}
\begin{minipage}[r]{0.48\linewidth}
% Set the overall layout of the tree
\tikzstyle{level 1}=[level distance=3cm, sibling distance=3.5cm]
\tikzstyle{level 2}=[level distance=3cm, sibling distance=2cm]
% Define styles for bags and leafs (sic!)
\tikzstyle{root} = [text width=0.8em, text centered]
\tikzstyle{bag} = [text width=0.8em, text centered]
\tikzstyle{end} = [minimum width=0pt, inner sep=0pt]
\begin{tikzpicture}[grow=down,auto]

\node[root] {$\Omega$} % root
child {
        node[bag] {$F$}                
            child {
                node[end, label=below:
                    $F\intersection S$] {}
                edge from parent
                node[above] {\brlab{$\frac{11}{13}$}}
            }
            child {
                node[end, label=below:
                    {$F\intersection\overline S$}] {}
                edge from parent
                node[above] {\brlab{$\frac{2}{13}$}}
            }
        edge from parent         
            node[above] {\brlab{}}
    }
child {
        node[bag] {$\overline F$}                    
            child {
                node[end, label=below:
                    {$\overline F\intersection S$}] {}
                edge from parent
                node[above] {\brlab{}}
            }
            child {
                node[end, label=below:
                    {$\overline F\intersection \overline S$}] {}
                edge from parent
                node[above] {\brlab{}}
            }
            edge from parent 
            node[above] {\brlab{}}
    };

\end{tikzpicture}
\vspace*{3mm}
\captionof{figure}{Inverser Wahrscheinlichkeitsbaum}
\end{minipage}

